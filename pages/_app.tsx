import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { useCallback, useState } from 'react'
import usePlayerList from '../helpers/usePlayerList'
import Head from 'next/head';
import { loadFull } from "tsparticles";
import { Engine } from 'tsparticles-engine';
import Particles from "react-tsparticles";

function MyApp({ Component, pageProps }: AppProps) {
  const playerList = usePlayerList();
  const particlesInit = useCallback(async (engine: Engine) => {
    console.log(engine);
    // you can initiate the tsParticles instance (engine) here, adding custom shapes or presets
    // this loads the tsparticles package bundle, it's the easiest method for getting everything ready
    // starting from v2 you can add only the features you need reducing the bundle size
    await loadFull(engine);
  }, []);

  const particlesLoaded = useCallback(async (container: any) => {
    await console.log(container);
  }, []);

  return (
    <>
      <Head>
        <title>Drinking Game</title>
      </Head>
      <Component {...pageProps} playerList={playerList} />
      <Particles
        id="tsparticles"
        init={particlesInit}
        loaded={particlesLoaded}
        options={{
          fullScreen: {
            enable: true,
            zIndex: -100
          },
          background: {
            color: {
              value: "#00688c",
            },
          },
          fpsLimit: 120,
          interactivity: {
            events: {
              onClick: {
                enable: false,
                mode: "push",
              },
              onHover: {
                enable: true,
                mode: "repulse",
              },
              resize: true,
            },
            modes: {
              grab: {
                distance: 400,
                line_linked: {
                  opacity: 1
                }
              },
              bubble: {
                distance: 400,
                size: 40,
                duration: 2,
                opacity: 0.8,
                speed: 3
              },
              repulse: {
                distance: 200
              },
              push: {
                particles_nb: 4
              },
              remove: {
                particles_nb: 2
              }
            },
          },
          emitters: {
            direction: "top",
            size: {
              width: 100,
              height: 0
            },
            position: {
              x: 50,
              y: 100
            },
            rate: {
              delay: 0.1,
              quantity: 2
            }
          },
          particles: {
            number: {
              value: 50,
              density: {
                enable: true,
                value_area: 800
              }
            },
            color: {
              value: "#ffffff"
            },
            shape: {
              type: "circle"
            },
            opacity: {
              value: 1,
              random: false,
              anim: {
                enable: false,
                speed: 3,
                opacity_min: 0.1,
                sync: false
              }
            },
            size: {
              value: 4,
              random: false,
              anim: {
                enable: true,
                speed: 3,
                size_min: 0.1,
                sync: true,
                startValue: "min",
                destroy: "none"
              }
            },
            line_linked: {
              enable: false,
              distance: 150,
              color: "#ffffff",
              opacity: 0.4,
              width: 1
            },
            move: {
              enable: true,
              speed: 5,
              direction: "top",
              random: false,
              straight: false,
              outModes: "destroy",
              attract: {
                enable: false,
                rotateX: 600,
                rotateY: 1200
              }
            }
          },
          detectRetina: true,
        }}
      />
    </>
  );
}

export default MyApp
