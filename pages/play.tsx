import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import { CardDataFileV1 } from '../helpers/CardDataFile';
import useFetchData from '../helpers/useFetchData';
import { PlayerListType } from '../helpers/usePlayerList';
import styles from '../styles/Play.module.css'

const Play: NextPage<{ playerList: PlayerListType }> = ({ playerList }) => {
    const router = useRouter();
    const [ players, addPlayer, removePlayer ] = playerList;
    const [currentTurnIndex, setCurrentTurnIndex] = useState(0);
    const [currentCardIndex, setCurrentCardIndex] = useState(0);

    useEffect(() => {
        if (players.length < 2) router.push("/");
    })

    const {
        data: cardsRaw,
        loading: cardsLoading,
        error: cardsError
    } = useFetchData("/drinking-game/data.json", {});

    // I hate TypeScript
    const data: CardDataFileV1 = (cardsRaw as unknown) as CardDataFileV1;

    function shuffleCard() {
        if (cardsRaw === null) return;
        setCurrentCardIndex(Math.floor(Math.random() * data.cards.length))
    }

    if (cardsLoading || cardsError) return (
        <div className="container">
            <div className="card">
                Loading game...
            </div>
        </div>
    );

    return (
        <div className="container">
            <div className="card">
                <h2>{players[currentTurnIndex]}&lsquo;s turn!</h2>
                <p>{data.cards[currentCardIndex].text}</p>
                <div className={styles.actionRow}>
                    <button onClick={() => {
                        if (currentTurnIndex === players.length - 1) setCurrentTurnIndex(0);
                        else setCurrentTurnIndex(currentTurnIndex + 1);
                        
                        shuffleCard();
                    }} className="styled-button">Next Turn</button>
                    <button onClick={() => shuffleCard()} className={styles.rerollButton + " styled-button"}>Re-roll</button>
                </div>
            </div>
        </div>
    )
}

export default Play
