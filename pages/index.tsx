import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import PlayerNameInput from '../components/PlayerNameInput'
import { PlayerListType } from '../helpers/usePlayerList'
import styles from '../styles/Home.module.css'

const PlayerSelection: NextPage<{playerList: PlayerListType}> = ({ playerList }) => {
  const router = useRouter();
  const [players, addPlayer, removePlayer] = playerList;

  const startGame = () => {
    router.push("/play");
  }

  /*
          <div className={styles.sourceUrlHeader}>
          Card URLs
        </div>
        <div className={styles.sourceUrl}>
          <div>https://google.com</div>
        </div>
        <br></br>
        */

  return (
    <div className="container">
      <div className="card">
        <h1>Drinking Game</h1>
        <div className={styles.sourceUrlHeader}>
          Players
        </div>
        <div className={styles.sourceUrl}>
          {players.map(p => <div key={p}>{p}</div>)}
        </div>
        <PlayerNameInput playerList={playerList}></PlayerNameInput>
        <button className="styled-button" onClick={startGame}>Get drunk</button>
      </div>
    </div>
  )
}

export default PlayerSelection
