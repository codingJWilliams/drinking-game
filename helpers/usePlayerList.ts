import { useState } from "react";

export default function usePlayerList() {
    const [ playerList, setPlayerList ] = useState<string[]>([]);

    return [
        playerList,
        (newPlayer: string) => {
            if (!playerList.includes(newPlayer)) setPlayerList([...playerList, newPlayer]);
        },
        (playerToDelete: string) => {
            setPlayerList(oldList => oldList.filter(it => it !== playerToDelete));
        }
    ];
}

export type PlayerListType = [string[], (newPlayer: string) => void, (playerToDelete: string) => void];