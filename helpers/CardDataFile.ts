export interface CardDataFile {
    version: number
}

export interface CardV1 {
    text: string
}

export interface CardDataFileV1 extends CardDataFile {
    cards: Array<CardV1>
}