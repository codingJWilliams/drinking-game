import { FetchEventResult } from "next/dist/server/web/types";
import { useEffect, useState } from "react";
const useFetchData = (
    fetchUrl: string,
    fetchOptions: RequestInit
) => {

    const localRequestConfig = fetchOptions || {};
    const [state, setState] = useState({
        loading: true,
        data: [],
        error: null,
    });


    useEffect(() => {
        fetch(fetchUrl, fetchOptions).then(async response => {
            const data = await response.json()
            setState(prevState => ({
                ...prevState,
                data
            }));
        }).catch(err => {
            setState(prevState => ({
                ...prevState,
                error: err,
            }))
        }).finally(() => {
            setState(prevState => ({
                ...prevState,
                loading: false,
            }))
        })
    }, [fetchOptions]);

    return state;
};
export default useFetchData;