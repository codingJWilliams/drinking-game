import { KeyboardEvent, ChangeEvent, useState } from "react"
import { PlayerListType } from "../helpers/usePlayerList";
import styles from "../styles/components/PlayerNameInput.module.css";

export default function PlayerNameInput({playerList} : { playerList: PlayerListType }) {
    const [players, addPlayer, removePlayer] = playerList;
    const [value, setValue] = useState("");

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setValue(event.target.value);
    }

    function save() {
        if (!value.length) return
        addPlayer(value);
        setValue("");
    }

    function handleKeyup(event: KeyboardEvent<HTMLInputElement>) {
        if (event.key === "Enter") save();
    }

    return (
        <>
            <div className={styles.container}>
                <input type="text" value={value} onChange={handleChange} onKeyUp={handleKeyup} placeholder="Add a player..." />
                <button onClick={save}>Submit</button>
            </div>
        </>
    )
}